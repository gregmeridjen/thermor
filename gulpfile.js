/**
 * Configuration for assets-builder
 * Documentation: https://github.com/gradientz/assets-builder
 */
require('./assets/builder')({

  sass: {
    src:   [
      'node_modules/normalize.css/normalize.css',
      'assets/styles/main.scss'
    ],
    watch: 'assets/styles/**/*.scss',
    dest:  'assets/dist/main.css',
    includePaths: ['node_modules'],
    browsers: ['last 3 versions', 'ie >= 11', 'ios >= 9', 'android >= 5']
  },

  jsconcat: [
    // minify and copy
    {
      src: 'node_modules/svg4everybody/dist/svg4everybody.js',
      dest: 'assets/dist/svg4everybody.js'
    },
    // concatenate, minify and copy
    {
      src: [
        'node_modules/jquery/dist/jquery.slim.min.js',
        'assets/scripts/*.js'
      ],
      watch: true,
      dest:  'assets/dist/main.js'
    }
  ],

  svgsymbols: {
    src: 'assets/icons/main/*.svg',
    watch: true,
    dest: 'assets/dist/main.svg',
    demo: true
  }

})
