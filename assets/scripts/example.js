/**
 * FIXME: give a short description of this component or feature
 */
;(function($) {
    'use strict';
 
    $(window).ready(init);
 
    // Constants
    var CONTAINER_SEL = '.Example';
    var ITEM_SEL = '.Example-item';
    var RESIZE_DELAY = 200;
 
    /**
     * Init
     */
    function init() {
        var container = $(CONTAINER_SEL);
        if (container.length === 1) {
            // Do some initial work: add event listeners,
            // call helper functions, etc.
            container.find(ITEM_SEL).each(doSomethingWithItem);
        }
    }
 
    /**
     * Do something with items
     */
    function doSomethingWithItem(index, item) {
        // …
    }
 
})(jQuery);
