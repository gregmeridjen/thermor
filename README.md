[FIXME: Project Name] front-end prototype
=========================================

Usage
-----

```sh
# Install dependencies
npm install

# Run a test server (with PHP & TwigExpress)
npm run server

# Build assets once
npm run build

# Watch changes and rebuild
npm run watch
```

Configuration
-------------

- `package.json`: add JS or CSS libs (generally from the command line, e.g. `npm install jquery`)
- `gulpfile.js`: configure CSS / JS / SVG compilation
- `twigexpress.json`: define global variables and namespaces

Target browsers
---------------

[FIXME: check the project’s requirements and update this section. See the [latest list of supported browsers](https://confluence.kaliop.net/x/mBEYAQ) but the project may have more specific requirements.]

*   Desktop: recent Chrome, Firefox, Safari and Edge
*   iOS: Safari 9+
*   Android: recent Chrome
*   IE support: 11+

Team standards and tools
------------------------

For HTML, CSS and JS code, please follow the Front-end Style Guide:<br>
https://confluence.kaliop.net/x/QYSg

This project uses [gradientz/twig-express](https://github.com/gradientz/twig-express) and [gradientz/assets-builder](https://github.com/gradientz/assets-builder).
